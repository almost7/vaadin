package com.packagename.myapp;
import com.packagename.myapp.vaadin.Todo;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

@Route
//@PWA(name = "Project Base for Vaadin Flow with Spring", shortName = "Project Base")
public class MainView extends VerticalLayout {

    public MainView(@Autowired MessageBean bean) {
        LoginOverlay component = new LoginOverlay();
        component.addLoginListener(e -> component.close());
        Button open = new Button("Login to READINESS",
                e -> component.setOpened(true));
        LoginI18n i18n = LoginI18n.createDefault();
        component.setTitle("LOGIN");
        component.setDescription("This the description i've chosen");
        i18n.setAdditionalInformation("To close the login form submit non-empty username and password");
        component.setI18n(i18n);
        add(component);
        this.add(open);
        this.setAlignItems(Alignment.CENTER);
        this.setHeightFull();
//        HorizontalLayout hl = new HorizontalLayout();
//        hl.setSizeFull();
//        hl.setAlignItems(Alignment.CENTER);
//        hl.add(open);
//        this.add(hl);
        FlexLayout flex = new FlexLayout();
        flex.add(open);
        flex.setSizeFull();
        flex.setAlignItems(Alignment.CENTER);
        flex.setJustifyContentMode(JustifyContentMode.CENTER);
        this.add(flex);
    }
}
