package com.packagename.myapp.vaadin;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

/**
 * Creator layout for creating todo items.
 */
@Tag("todo-creator")
@HtmlImport("frontend://components/TodoCreator.html")
class TodoCreator extends PolymerTemplate<TemplateModel> {

    private Random rand = new Random(System.currentTimeMillis());
    private List<CreateCallback> callbacks = new ArrayList<>(0);

    /**
     * Add a creation callback to listen to for newly created todo items.
     *
     * @param callback
     *            creation callback
     */
    public void addCreateCallback(CreateCallback callback) {
        callbacks.add(callback);
    }

    @ClientCallable
    private void createTodo(String task, String user) {
        Todo todo = new Todo();
        todo.setTask(task);
        todo.setUser(user);
        todo.setTime(LocalDateTime.now());
        todo.setCompleted(false);
        todo.setRid(rand.nextInt());

        callbacks.forEach(callback -> callback.createdNewTodo(todo));
    }

    /**
     * Creation callback interface.
     */
    @FunctionalInterface
    public interface CreateCallback extends Serializable {
        /**
         * Method called when a new {@link Todo} item is created.
         *
         * @param todo
         *            the created {@link Todo} item
         */
        void createdNewTodo(Todo todo);
    }

}