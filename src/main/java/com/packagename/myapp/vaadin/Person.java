package com.packagename.myapp.vaadin;

import java.util.ArrayList;
import java.util.List;

public class Person {

    public String Name;
    public String Email;

    public String getEmail() {
        return Email;
    }

    public Person(String name, String email){
        this.Email = email;
        this.Name = name;
    }

    public void setEmail(String email) {
        Email = email;
    }
    public java.lang.String getName() {
        return Name;
    }
    public void setName(String name) {
        Name = name;
    }

    public static List<Person> CreateItem (){
        List<Person> list = new ArrayList<>();
        list.add(new Person("Carlos", "carlos@email.com"));
        list.add(new Person("Miguel", "miguel@email.com"));
        return list;
    }
}
