package com.packagename.myapp.vaadin;
import com.packagename.myapp.MessageBean;
import com.packagename.myapp.vaadin.Todo;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.gridpro.GridPro;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("grid")
public class gridview extends VerticalLayout {

    public gridview(@Autowired MessageBean bean) {

        GridPro<Person> grid = new GridPro<>();

        grid.setItems(Person.CreateItem());
//        grid.addColumn(Person::getName).setHeader("Name");
//        grid.addColumn(Person::getEmail).setHeader("Email");
        /*
         * It is possible to discard edit mode when moving to the next cell by using grid pro method setSingleCellEdit.
         */
        grid.setSingleCellEdit(true);

        grid.addEditColumn(Person::getName)
                .text((item, newValue) ->
                        item.setName(newValue))
                .setHeader("Name (editable)");

        grid.addEditColumn(Person::getEmail)
                .text((item, newValue) ->
                        item.setEmail(newValue))
                .setHeader("Email (editable)");

//        grid.addEditColumn(Person::isSubscriber)
//                .checkbox((item, newValue) ->
//                        item.setSubscriber(newValue))
//                .setHeader("Subscriber (editable)");
        add(grid);
    }
}