
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Todo item.
 */
public class Todo implements Serializable {

    private String task;
    private String user;
    private int rid;

    private LocalDateTime time;

    private boolean completed = false;


    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return String.format("Task: %s, User: %s, Id: %s", task,
                user, rid);
    }
}